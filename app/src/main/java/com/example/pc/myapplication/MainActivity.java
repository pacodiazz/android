package com.example.pc.myapplication;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.pc.myapplication.pantalla;

public class MainActivity extends AppCompatActivity {

    private Button btnNotificacion;

    private static final int NOTIF_ALERTA_ID = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //toast
 toast("hola prros");
 //alerta
 alertadedialogo("Hola?");
//boton de
        Button botonprueba = (Button) findViewById(R.id.button2);
        botonprueba.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                toast("hola ");
            }
        });
//termina boton
        //boton de vibrar
        Button vibrar = (Button) findViewById(R.id.button3);
        vibrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Vibrator(400);
            }
        });
//termina boton
        final Button pantalla = (Button) findViewById(R.id.pantalla);
        pantalla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


            }
        });
//alertas en
        btnNotificacion = (Button)findViewById(R.id.button4);
        btnNotificacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(MainActivity.this)
                                .setSmallIcon(android.R.drawable.stat_sys_warning)
                                .setContentTitle("Mensaje de Alerta")
                                .setContentText("Ejemplo de notificación.")
                                .setContentInfo("4")
                                .setTicker("Alerta!");
                Intent notIntent =
                        new Intent(MainActivity.this, MainActivity.class);
                PendingIntent contIntent = PendingIntent.getActivity(
                        MainActivity.this, 0, notIntent, 0);
                mBuilder.setContentIntent(contIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(NOTIF_ALERTA_ID, mBuilder.build());
            }
        });
//alertas termina

//texto desde java
        TextView myAwesomeTextView = (TextView)findViewById(R.id.text);
        myAwesomeTextView.setText("TextView desde java");
//termina texo desde java

        //pantalla completa
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //termina pantalla completa
    }
//toast
public void toast(String texo){
    Context context = getApplicationContext();
    int duration = Toast.LENGTH_SHORT;
    Toast toast = Toast.makeText(context, texo, duration);
    toast.show();
}
//termina toast
// mensaje de dialgo
    private void alertadedialogo( String Mensaje ) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle( "hola " )
                .setMessage(Mensaje)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }
// termina
    private void Vibrator(int segundos){
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
// Vibrate for 400 milliseconds
        v.vibrate(segundos);
    }



}
